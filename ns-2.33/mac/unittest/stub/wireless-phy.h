/* dummy */
class WirelessPhy : public Phy {
public:
    WirelessPhy() { isWakeup = true; }

    void node_wakeup() { isWakeup = true; }
    void node_sleep() { isWakeup = false; }

    bool Is_sleeping() { return !isWakeup; }

    bool isWakeup;
};
