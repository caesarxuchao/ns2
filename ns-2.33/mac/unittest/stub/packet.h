#ifndef _packet_h
#define _packet_h

#include <string.h>
#include <assert.h>
#include <stdio.h>
#include <stdlib.h>

#include "testdummy.h"
#include "address.h"

#define HDR_CMN(p)      (hdr_cmn::access(p))
#define HDR_ARP(p)      (hdr_arp::access(p))
#define HDR_MAC(p)      (hdr_mac::access(p))
#define HDR_MAC802_11(p) ((hdr_mac802_11 *)hdr_mac::access(p))
#define HDR_MAC_TDMA(p) ((hdr_mac_tdma *)hdr_mac::access(p))
#define HDR_SMAC(p)     ((hdr_smac *)hdr_mac::access(p))
#define HDR_LL(p)       (hdr_ll::access(p))
#define HDR_HDLC(p)     ((hdr_hdlc *)hdr_ll::access(p))
#define HDR_IP(p)       (hdr_ip::access(p))
#define HDR_RTP(p)      (hdr_rtp::access(p))
#define HDR_TCP(p)      (hdr_tcp::access(p))
#define HDR_SCTP(p)     (hdr_sctp::access(p))
#define HDR_SR(p)       (hdr_sr::access(p))
#define HDR_TFRC(p)     (hdr_tfrc::access(p))
#define HDR_TORA(p)     (hdr_tora::access(p))
#define HDR_IMEP(p)     (hdr_imep::access(p))
#define HDR_CDIFF(p)    (hdr_cdiff::access(p))  /* chalermak's diffusion*/
//#define HDR_DIFF(p)     (hdr_diff::access(p))  /* SCADD's diffusion ported into ns */
#define HDR_LMS(p)		(hdr_lms::access(p))

typedef unsigned int packet_t;

static const packet_t PT_TCP = 0;
static const packet_t PT_UDP = 1;
static const packet_t PT_CBR = 2;
static const packet_t PT_AUDIO = 3;
static const packet_t PT_VIDEO = 4;
static const packet_t PT_ACK = 5;
static const packet_t PT_START = 6;
static const packet_t PT_STOP = 7;
static const packet_t PT_PRUNE = 8;
static const packet_t PT_GRAFT = 9;
static const packet_t PT_GRAFTACK = 10;
static const packet_t PT_JOIN = 11;
static const packet_t PT_ASSERT = 12;
static const packet_t PT_MESSAGE = 13;
static const packet_t PT_RTCP = 14;
static const packet_t PT_RTP = 15;
static const packet_t PT_RTPROTO_DV = 16;
static const packet_t PT_CtrMcast_Encap = 17;
static const packet_t PT_CtrMcast_Decap = 18;
static const packet_t PT_SRM = 19;
        /* simple signalling messages */
static const packet_t PT_REQUEST = 20;
static const packet_t PT_ACCEPT = 21;
static const packet_t PT_CONFIRM = 22;
static const packet_t PT_TEARDOWN = 23;
static const packet_t PT_LIVE = 24;   // packet from live network
static const packet_t PT_REJECT = 25;

static const packet_t PT_TELNET = 26; // not needed: telnet use TCP
static const packet_t PT_FTP = 27;
static const packet_t PT_PARETO = 28;
static const packet_t PT_EXP = 29;
static const packet_t PT_INVAL = 30;
static const packet_t PT_HTTP = 31;

        /* new encapsulator */
static const packet_t PT_ENCAPSULATED = 32;
static const packet_t PT_MFTP = 33;

        /* CMU/Monarch's extnsions */
static const packet_t PT_ARP = 34;
static const packet_t PT_MAC = 35;
static const packet_t PT_TORA = 36;
static const packet_t PT_DSR = 37;
static const packet_t PT_AODV = 38;
static const packet_t PT_IMEP = 39;
        
        // RAP packets
static const packet_t PT_RAP_DATA = 40;
static const packet_t PT_RAP_ACK = 41;
  
static const packet_t PT_TFRC = 42;
static const packet_t PT_TFRC_ACK = 43;
static const packet_t PT_PING = 44;
        
static const packet_t PT_PBC = 45;
        // Diffusion packets - Chalermek
static const packet_t PT_DIFF = 46;
        
        // LinkState routing update packets
static const packet_t PT_RTPROTO_LS = 47;
        
        // MPLS LDP header
static const packet_t PT_LDP = 48;
        
        // GAF packet
static const packet_t PT_GAF = 49;
        
        // ReadAudio traffic
static const packet_t PT_REALAUDIO = 50;
        
        // Pushback Messages
static const packet_t PT_PUSHBACK = 51;
  
  #ifdef HAVE_STL
        // Pragmatic General Multicast
static const packet_t PT_PGM = 52;
  #endif //STL
        // LMS packets
static const packet_t PT_LMS = 53;
static const packet_t PT_LMS_SETUP = 54;

static const packet_t PT_SCTP = 55;
static const packet_t PT_SCTP_APP1 = 56;

        // SMAC packet
static const packet_t PT_SMAC = 57;
        // XCP packet
static const packet_t PT_XCP = 58;

        // HDLC packet
static const packet_t PT_HDLC = 59;

        // Bell Labs Traffic Trace Type (PackMime OL)
static const packet_t PT_BLTRACE = 60;

        // insert new packet types here
static const packet_t       PT_NTYPE = 61; // This MUST be the LAST one

enum packetClass
{
	UNCLASSIFIED,
	ROUTING,
	DATApkt
};


class p_info {
public:
	p_info()
	{
		initName();
	}
	const char* name(packet_t p) const { 
		if ( p <= p_info::nPkt_ ) return name_[p];
		return 0;
	}
	static bool data_packet(packet_t type) {
		return ( (type) == PT_TCP || \
		         (type) == PT_TELNET || \
		         (type) == PT_CBR || \
		         (type) == PT_AUDIO || \
		         (type) == PT_VIDEO || \
		         (type) == PT_ACK || \
		         (type) == PT_SCTP || \
		         (type) == PT_SCTP_APP1 || \
		         (type) == PT_HDLC \
		        );
	}
	static packetClass classify(packet_t type) {		
		if (type == PT_DSR || 
		    type == PT_MESSAGE || 
		    type == PT_TORA || 
		    type == PT_AODV)
			return ROUTING;		
		if (type == PT_TCP || 
		    type == PT_TELNET || 
		    type == PT_CBR || 
		    type == PT_AUDIO || 
		    type == PT_VIDEO || 
		    type == PT_ACK || 
		    type == PT_SCTP || 
		    type == PT_SCTP_APP1 || 
		    type == PT_HDLC)
			return DATApkt;
		return UNCLASSIFIED;
	}

	static void initName()
	{
		if(nPkt_ >= PT_NTYPE+1)
		        return;
		char **nameNew = new char*[PT_NTYPE+1];
		for(unsigned int i = (unsigned int)PT_SMAC+1; i < nPkt_; i++)
		{
		        nameNew[i] = name_[i];
		}
		if(!nPkt_)
		        delete [] name_;
		name_ = nameNew;
		nPkt_ = PT_NTYPE+1;
		

		name_[PT_TCP]= "tcp";
		name_[PT_UDP]= "udp";
		name_[PT_CBR]= "cbr";
		name_[PT_AUDIO]= "audio";
		name_[PT_VIDEO]= "video";
		name_[PT_ACK]= "ack";
		name_[PT_START]= "start";
		name_[PT_STOP]= "stop";
		name_[PT_PRUNE]= "prune";
		name_[PT_GRAFT]= "graft";
		name_[PT_GRAFTACK]= "graftAck";
		name_[PT_JOIN]= "join";
		name_[PT_ASSERT]= "assert";
		name_[PT_MESSAGE]= "message";
		name_[PT_RTCP]= "rtcp";
		name_[PT_RTP]= "rtp";
		name_[PT_RTPROTO_DV]= "rtProtoDV";
		name_[PT_CtrMcast_Encap]= "CtrMcast_Encap";
		name_[PT_CtrMcast_Decap]= "CtrMcast_Decap";
		name_[PT_SRM]= "SRM";
	
		name_[PT_REQUEST]= "sa_req";	
		name_[PT_ACCEPT]= "sa_accept";
		name_[PT_CONFIRM]= "sa_conf";
		name_[PT_TEARDOWN]= "sa_teardown";
		name_[PT_LIVE]= "live"; 
		name_[PT_REJECT]= "sa_reject";
	
		name_[PT_TELNET]= "telnet";
		name_[PT_FTP]= "ftp";
		name_[PT_PARETO]= "pareto";
		name_[PT_EXP]= "exp";
		name_[PT_INVAL]= "httpInval";
		name_[PT_HTTP]= "http";
		name_[PT_ENCAPSULATED]= "encap";
		name_[PT_MFTP]= "mftp";
		name_[PT_ARP]= "ARP";
		name_[PT_MAC]= "MAC";
		name_[PT_TORA]= "TORA";
		name_[PT_DSR]= "DSR";
		name_[PT_AODV]= "AODV";
		name_[PT_IMEP]= "IMEP";

		name_[PT_RAP_DATA] = "rap_data";
		name_[PT_RAP_ACK] = "rap_ack";

 		name_[PT_TFRC]= "tcpFriend";
		name_[PT_TFRC_ACK]= "tcpFriendCtl";
		name_[PT_PING]="ping";
	
		name_[PT_PBC] = "PBC";

	 	/* For diffusion : Chalermek */
 		name_[PT_DIFF] = "diffusion";

		// Link state routing updates
		name_[PT_RTPROTO_LS] = "rtProtoLS";

		// MPLS LDP packets
		name_[PT_LDP] = "LDP";

		// for GAF
                name_[PT_GAF] = "gaf";      

		// RealAudio packets
		name_[PT_REALAUDIO] = "ra";

		//pushback 
		name_[PT_PUSHBACK] = "pushback";

#ifdef HAVE_STL
		// for PGM
		name_[PT_PGM] = "PGM";
#endif //STL

		// LMS entries
		name_[PT_LMS]="LMS";
		name_[PT_LMS_SETUP]="LMS_SETUP";

		name_[PT_SCTP]= "sctp";
 		name_[PT_SCTP_APP1] = "sctp_app1";
		
		// smac
		name_[PT_SMAC]="smac";

		// HDLC
		name_[PT_HDLC]="HDLC";

		// XCP
		name_[PT_XCP]="xcp";

		// Bell Labs (PackMime OL)
		name_[PT_BLTRACE]="BellLabsTrace";
		
		name_[PT_NTYPE]= "undefined";
	}
	static int addPacket(char *name);
	static packet_t getType(const char *name)
	{
		for(unsigned int i = 0; i < nPkt_; i++)
		{
		        if(strcmp(name, name_[i]) == 0)
		                return i;
		}
		return PT_NTYPE;

	}
private:
	static char** name_;
	static unsigned int nPkt_;
};
extern p_info packet_info; /* map PT_* to string name */

class AppData {};

class PacketStamp {
public:
    double RxPr;
    double CPThresh;
};

class Packet;
typedef void (*FailureCallback)(Packet *,void *);

class Packet : public Event {
private:
	unsigned char* bits_;	// header bits
	AppData* data_;		// variable size buffer for 'data'
	static void init(Packet*);     // initialize pkt hdr 
	bool fflag_;
protected:
	static Packet* free_;	// packet free list
	int	ref_count_;	// free the pkt until count to 0
public:
	Packet* next_;		// for queues and the free list
	static int hdrlen_;

	Packet() : bits_(0), data_(0), ref_count_(0), next_(0) { }
	inline unsigned char* const bits() { return (bits_); }
	inline Packet* copy() const;
	inline Packet* refcopy() { ++ref_count_; return this; }
	inline int& ref_count() { return (ref_count_); }
	static inline Packet* alloc();
	static inline Packet* alloc(int);
	inline void allocdata(int);
	// dirty hack for diffusion data
	inline void initdata() { data_  = 0;}
	static inline void free(Packet*);
	inline unsigned char* access(int off) const {
		if (off < 0)
			abort();
		return (&bits_[off]);
	}

	// Monarch extn

	static void dump_header(Packet *p, int offset, int length);

	// the pkt stamp carries all info about how/where the pkt
        // was sent needed for a receiver to determine if it correctly
        // receives the pkt
        PacketStamp	txinfo_;  

	/*
         * According to cmu code:
	 * This flag is set by the MAC layer on an incoming packet
         * and is cleared by the link layer.  It is an ugly hack, but
         * there's really no other way because NS always calls
         * the recv() function of an object.
	 * 
         */
        u_int8_t        incoming;

	//monarch extns end;
};

enum ns_af_enum { NS_AF_NONE, NS_AF_ILINK, NS_AF_INET };
//enum ModulationScheme {BPSK = 0, QPSK = 1, QAM16 = 2, QAM64 = 3};

struct hdr_cmn {
	enum dir_t { DOWN= -1, NONE= 0, UP= 1 };
	packet_t ptype_;	// packet type (see above)
	int	size_;		// simulated packet size
	int	uid_;		// unique id
	int	error_;		// error flag
	double	ts_;		// timestamp: for q-delay measurement
	int	iface_;		// receiving interface (label)
	dir_t	direction_;	// direction: 0=none, 1=up, -1=down

	//Monarch extn begins
	nsaddr_t prev_hop_;     // IP addr of forwarding hop
	nsaddr_t next_hop_;	// next hop for this packet
	int      addr_type_;    // type of next_hop_ addr
	nsaddr_t last_hop_;     // for tracing on multi-user channels

        FailureCallback xmit_failure_;
        void *xmit_failure_data_;

	// tx time for this packet in sec
	int xmit_reason_;
#define XMIT_REASON_RTS 0x01
#define XMIT_REASON_ACK 0x02

	int num_forwards_;

	double txtime_;
	inline double& txtime() { return(txtime_); }

	static int offset_;	// offset for this header
	inline static int& offset() { return offset_; }
	inline static hdr_cmn* access(const Packet* p) {
		return (hdr_cmn*) p->access(offset_);
	}
	
        /* per-field member functions */
	inline packet_t& ptype() { return (ptype_); }
	inline int& size() { return (size_); }
	inline int& uid() { return (uid_); }
	inline int& error() { return error_; }
	inline double& timestamp() { return (ts_); }
	inline int& iface() { return (iface_); }
	inline dir_t& direction() { return (direction_); }

	// monarch_begin
	inline nsaddr_t& next_hop() { return (next_hop_); }
	inline int& addr_type() { return (addr_type_); }
	inline int &num_forwards() { return num_forwards_; }
};

class PacketHeaderClass : public TclClass {
public:
    PacketHeaderClass(const char *classname, int hdrsize)
        : TclClass(classname)
        {
            hdrlen_ = hdrsize;
            offset_ = 0;
        }

    inline void bind_offset(int *off) { offset_ = off; }
    inline void offset(int *off) { offset_ = off; }

    int hdrlen_;
    int *offset_;
};

inline void Packet::init(Packet* p)
{
	bzero(p->bits_, hdrlen_);
}

inline Packet* Packet::alloc()
{
	Packet* p = free_;
	if (p != 0) {
		assert(p->fflag_ == FALSE);
		free_ = p->next_;
		assert(p->data_ == 0);
		p->uid_ = 0;
		p->time_ = 0;
	} else {
		p = new Packet;
		p->bits_ = new unsigned char[hdrlen_];
		if (p == 0 || p->bits_ == 0)
			abort();
	}
	init(p); // Initialize bits_[]
	(HDR_CMN(p))->next_hop_ = -2; // -1 reserved for IP_BROADCAST
	(HDR_CMN(p))->last_hop_ = -2; // -1 reserved for IP_BROADCAST
	p->fflag_ = TRUE;
	(HDR_CMN(p))->direction() = hdr_cmn::DOWN;
	/* setting all direction of pkts to be downward as default; 
	   until channel changes it to +1 (upward) */
	p->next_ = 0;
	return (p);
}

/* 
 * Allocate an n byte data buffer to an existing packet 
 * 
 * To set application-specific AppData, use Packet::setdata()
 */
inline void Packet::allocdata(int n)
{
}

/* allocate a packet with an n byte data buffer */
inline Packet* Packet::alloc(int n)
{
	Packet* p = alloc();
	if (n > 0) 
		p->allocdata(n);
	return (p);
}


inline void Packet::free(Packet* p)
{
	if (p->fflag_) {
		if (p->ref_count_ == 0) {
			/*
			 * A packet's uid may be < 0 (out of a event queue), or
			 * == 0 (newed but never gets into the event queue.
			 */
			assert(p->uid_ <= 0);
			// Delete user data because we won't need it any more.
			if (p->data_ != 0) {
				delete p->data_;
				p->data_ = 0;
			}
			init(p);
			p->next_ = free_;
			free_ = p;
			p->fflag_ = FALSE;
		} else {
			--p->ref_count_;
		}
	}
}

inline Packet* Packet::copy() const
{
	
	Packet* p = alloc();
	memcpy(p->bits(), bits_, hdrlen_);

//	p->txinfo_.init(&txinfo_);
 
	return (p);
}

inline void
Packet::dump_header(Packet *p, int offset, int length)
{
        assert(offset + length <= p->hdrlen_);
        struct hdr_cmn *ch = HDR_CMN(p);

        fprintf(stderr, "\nPacket ID: %d\n", ch->uid());

        for(int i = 0; i < length ; i+=16) {
                fprintf(stderr, "%02x %02x %02x %02x %02x %02x %02x %02x %02x %02x %02x %02x %02x %02x %02x %02x\n",
                        p->bits_[offset + i],     p->bits_[offset + i + 1],
                        p->bits_[offset + i + 2], p->bits_[offset + i + 3],
                        p->bits_[offset + i + 4], p->bits_[offset + i + 5],
                        p->bits_[offset + i + 6], p->bits_[offset + i + 7],
                        p->bits_[offset + i + 8], p->bits_[offset + i + 9],
                        p->bits_[offset + i + 10], p->bits_[offset + i + 11],
                        p->bits_[offset + i + 12], p->bits_[offset + i + 13],
                        p->bits_[offset + i + 14], p->bits_[offset + i + 15]);
        }
}

#endif
