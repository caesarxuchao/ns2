#ifndef _testdummy_h
#define _testdummy_h

#include <assert.h>
#include <string.h>
#include <sys/types.h>
#include <stdarg.h>

#define FALSE 0
#define TRUE (!FALSE)

#define TCL_OK 0
#define TCL_ERROR 1

class Tcl {
public:
    static Tcl& instance() { return the_instance; }

    void evalf(const char *) {};
    const char * result() { return "0"; }

private:
    static Tcl the_instance;
};

class TclClass {
public:
    TclClass(const char *) {};
};

class TclObject {
public:
    void bind(const char *name, u_int32_t *) {};
    void bind(const char *name, double *) {};
    void bind_bw(const char *name, double *) {};
    void bind_bool(const char *name, int *) {};

    TclObject *lookup(const char *name) { return NULL; }
};

// lib/bsd-list.h
#define LIST_ENTRY(type) \
struct { \
    type *le_next; \
    type **le_prev; \
}

// scheduler.h
class Event {
public:
    Event() {
        uid_ = 0;
        time_ = 0.0;
    }
    int uid_;
    double time_;
    //Handler *handler_;
};

class Handler {
public:
    virtual void handle(Event *e) {};
};

class Scheduler {
public:
    static Scheduler & instance() {
        return the_scheduler;
    }

    double clock() {
        return 0.0;
    }

    void schedule(Handler *h, Event *e, double time) {};
    void start(double t) {};
    void cancel(Event *e) {};

private:
    static Scheduler the_scheduler;
};

// common/object.h
class Packet;
class NsObject : public TclObject, public Handler 
{
public:
    void transmit(Packet *, double) {};
    void recv(Packet *, Handler *) {};
};

// common/bi-connector.h
class BiConnector : public NsObject
{
public:
    NsObject *downtarget_;
    NsObject *uptarget_;

    void drop(Packet *p) {};
    void drop(Packet *p, const char *s) {};
};

// mac/phy.h
class Node;
class Phy : public BiConnector {
public:
    Node* node(void) { return node_; }
    Node* node_;
};

#define TIME_FORMAT "%f"

// trace/bassetrace.h
class EventTrace 
{
public:
    char* buffer() { return buf; }
    char* nbuffer() { return nbuf; }

    double round(double x) { return x; }
    void dump() {};

private: 
    char buf[1024];
    char nbuf[1024];
};

// mobile/energy-model.h
class EnergyModel {
public:
    enum SleepState { WAITING, POWERSAVING, INROUTE };
    inline EnergyModel() { 
        sleep_mode_ = 0;
        state_ = WAITING;
    }

    int sleep() { return sleep_mode_; }
    int state() { return state_; }
    void set_node_state(SleepState s) { state_ = s; }
    void set_node_sleep(int x) { sleep_mode_ = x; }
    int adaptivefidelity() { return 0; };
    void add_neighbor(u_int32_t x) {};

    int sleep_mode_;
    int state_;

};

// common/node.h
class Node {
public:
    EnergyModel *energy_model() { return &energy_model_; }

    EnergyModel energy_model_;
};

void initHeaders();
class Mac802_11;
Mac802_11 *allocMac();
void freeMac(Mac802_11 *mac);

#endif
