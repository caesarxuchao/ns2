#ifndef _queue_h
#define _queue_h

class PacketQueue {
public:
    PacketQueue() {
        head = NULL; tail = NULL; len = 0;
    }

    int length() { return len; }

    Packet *enque(Packet *p) {
        Packet *pt = tail;
        if (!tail) {
            head = tail = p;
        } else {
            tail->next_ = p;
            tail = p;
        }
        p->next_ = 0;
        len++;
        return pt;
    }
    Packet *deque() {
        if (!head) return 0;
        Packet *p = head;
        head = p->next_;
        if (!head) tail = 0;
        len--;
        return p;
    }
        
    Packet *head, *tail;
    int len;
};

#endif
