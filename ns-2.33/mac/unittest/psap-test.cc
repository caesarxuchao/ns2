/* -*-  Mode:C++; c-basic-offset:8; tab-width:8; indent-tabs-mode:t -*- */
// PS_AP クラステスト

#include <cppunit/extensions/HelperMacros.h>

#include "testdummy.h"
#include "packet.h"
#define private public
#define protected public
#include "mac.h"
#include "mac-802_11.h"
#include "mac-802_11-psap.h"
#undef private
#undef protected

class PS_AP_Test : public CPPUNIT_NS::TestFixture
{
	CPPUNIT_TEST_SUITE( PS_AP_Test );
	CPPUNIT_TEST(testCreate);
	CPPUNIT_TEST(testPVB);
	CPPUNIT_TEST(testAllocateAid);
	CPPUNIT_TEST(testSetupBeacon1);
	CPPUNIT_TEST(testSetupBeacon2);
	CPPUNIT_TEST(testSetupBeacon3);
	CPPUNIT_TEST(testUpdatePSmode);
	CPPUNIT_TEST(testRecvPSPoll);
	CPPUNIT_TEST(testEnqueuePSPacket1);
	CPPUNIT_TEST(testEnqueuePSPacket2);
	CPPUNIT_TEST(testEnqueuePSPacket3);
	CPPUNIT_TEST(testDequeuePSPacket1);
	CPPUNIT_TEST(testDequeuePSPacket2);
	CPPUNIT_TEST_SUITE_END();

private:
	Mac802_11 *mac;
	PS_AP *target;

public:
	void setUp();
	void tearDown();

	void testCreate();
	void testPVB();
	void testAllocateAid();
	void testSetupBeacon1();
	void testSetupBeacon2();
	void testSetupBeacon3();
	void testRecvPSPoll();
	void testEnqueuePSPacket1();
	void testEnqueuePSPacket2();
	void testEnqueuePSPacket3();
	void testDequeuePSPacket1();
	void testDequeuePSPacket2();
	void testUpdatePSmode();

};
CPPUNIT_TEST_SUITE_REGISTRATION(PS_AP_Test);

////////////////////////////////////////////////////////////////////////////

void PS_AP_Test::setUp()
{
	mac = allocMac();
	target = &mac->psap;
}

void PS_AP_Test::tearDown()
{
	freeMac(mac);
}

// 生成テスト
void PS_AP_Test::testCreate()
{
	CPPUNIT_ASSERT_EQUAL(mac, target->mac_);
}

// PVB テスト
void PS_AP_Test::testPVB()
{
	struct beacon_frame bf;
	int i;

	// 全クリア
	bf.clear_pvb();
	for (i = 1; i <= MAX_AID; i++) {
		CPPUNIT_ASSERT(!bf.is_pvb_set(i));
	}

	// 1, MAX_AID テスト
	bf.set_pvb(1);
	CPPUNIT_ASSERT(bf.is_pvb_set(1));
    
	bf.clear_pvb();
	bf.set_pvb(MAX_AID);
	CPPUNIT_ASSERT(bf.is_pvb_set(MAX_AID));
}


// AID 割り当てテスト
void PS_AP_Test::testAllocateAid()
{
	struct client_table *e;

	// STA 作成
	mac->update_client_table(0, 1, 1);
	mac->update_client_table(1, 1, 1);

	// AID 割り当て
	CPPUNIT_ASSERT(target->allocateAid(0) == 1);
	CPPUNIT_ASSERT(target->allocateAid(1) == 2);
	CPPUNIT_ASSERT(target->allocateAid(0) == 3);

	// AID チェック
	e = mac->find_client_entry(0);
	CPPUNIT_ASSERT(e != NULL);
	CPPUNIT_ASSERT(e->aid == 3);

	e = mac->find_client_entry(1);
	CPPUNIT_ASSERT(e != NULL);
	CPPUNIT_ASSERT(e->aid == 2);
}

// ビーコン作成テスト (パケットなし)
void PS_AP_Test::testSetupBeacon1()
{
	struct beacon_frame bf;
	int dtimPeriod = mac->phymib_.getDTIMPeriod();

	target->DTIMcount_ = 0;

	// DTIM
	target->setupBeacon(&bf);

	CPPUNIT_ASSERT(bf.bf_dtimcount == 0);
	CPPUNIT_ASSERT(bf.bf_dtimperiod == dtimPeriod);
	CPPUNIT_ASSERT(bf.bf_bitmapcontrol == 0);
	CPPUNIT_ASSERT(!target->sendingPBcast_);

	// TIM after DTIM
	target->setupBeacon(&bf);

	CPPUNIT_ASSERT(bf.bf_dtimcount == dtimPeriod - 1);
	CPPUNIT_ASSERT(bf.bf_dtimperiod == dtimPeriod);
	CPPUNIT_ASSERT(bf.bf_bitmapcontrol == 0);
	CPPUNIT_ASSERT(!target->sendingPBcast_);
}

// ビーコン作成テスト (ブロードキャストキューあり)
void PS_AP_Test::testSetupBeacon2()
{
	struct beacon_frame bf;
	int dtimPeriod = mac->phymib_.getDTIMPeriod();

	// enqueuue broadcast
	Packet *p = Packet::alloc();
	target->bcast_queue_.enque(p);

	target->DTIMcount_ = 0;

	// DTIM
	target->setupBeacon(&bf);

	CPPUNIT_ASSERT(bf.bf_dtimcount == 0);
	CPPUNIT_ASSERT(bf.bf_dtimperiod == dtimPeriod);
	CPPUNIT_ASSERT(bf.bf_bitmapcontrol == 1);
	CPPUNIT_ASSERT(target->sendingPBcast_);
}

// ビーコン作成テスト (unicast キューあり)
void PS_AP_Test::testSetupBeacon3()
{
	struct beacon_frame bf;
	int dtimPeriod = mac->phymib_.getDTIMPeriod();

	// create client
	mac->update_client_table(10, 1, 1); // create client entry
	struct client_table *e = mac->find_client_entry(10);
	e->aid = 20;
	e->isPowerSave = true;
    
	Packet *p = Packet::alloc();
	e->pktQueue.enque(p);

	target->DTIMcount_ = 0;

	// DTIM
	target->setupBeacon(&bf);

	CPPUNIT_ASSERT(bf.bf_dtimcount == 0);
	CPPUNIT_ASSERT(bf.bf_dtimperiod == dtimPeriod);
	CPPUNIT_ASSERT(bf.bf_bitmapcontrol == 0);
	CPPUNIT_ASSERT(!bf.is_pvb_set(1));
	CPPUNIT_ASSERT(bf.is_pvb_set(20));
	CPPUNIT_ASSERT(!target->sendingPBcast_);
}

// Update PS mode テスト
void PS_AP_Test::testUpdatePSmode()
{
	u_int32_t src = 10;

	// create client table
	mac->update_client_table(src, 1, 1);
	struct client_table *e = mac->find_client_entry(src);

	struct frame_control fc;
    
	fc.fc_protocol_version = MAC_ProtocolVersion;
	fc.fc_type       = MAC_Type_Control;
	fc.fc_subtype    = MAC_Subtype_PSPoll;
	fc.fc_to_ds      = 0;
	fc.fc_from_ds    = 0;
	fc.fc_more_frag  = 0;
	fc.fc_retry      = 0;
	fc.fc_more_data  = 0;
	fc.fc_wep        = 0;
	fc.fc_order      = 0;

	// power save on
	fc.fc_pwr_mgt    = 1;
	target->updatePSmode(fc, src);
	CPPUNIT_ASSERT(e->isPowerSave);

	// power save off
	fc.fc_pwr_mgt    = 0;
	target->updatePSmode(fc, src);
	CPPUNIT_ASSERT(!e->isPowerSave);

}

// PS Poll 受信テスト
void PS_AP_Test::testRecvPSPoll()
{
	u_int32_t src = 2;

	// create client table
	mac->update_client_table(src, 1, 1);
	struct client_table *e = mac->find_client_entry(src);
	e->aid = 10;
	e->isPolled = false;

	// enqueue one packet
	Packet *p = Packet::alloc();
	e->pktQueue.enque(p);

	// create PS-Poll

	// miyamoto add
	Packet *psp = Packet::alloc();
       	struct pspoll_frame *pspf = (struct pspoll_frame*)psp->access(hdr_mac::offset_);
		
  	pspf->pspf_fc.fc_protocol_version = MAC_ProtocolVersion;
	pspf->pspf_fc.fc_type       = MAC_Type_Control;
 	pspf->pspf_fc.fc_subtype    = MAC_Subtype_PSPoll;
 	pspf->pspf_fc.fc_to_ds      = 0;
 	pspf->pspf_fc.fc_from_ds    = 0;
 	pspf->pspf_fc.fc_more_frag  = 0;
 	pspf->pspf_fc.fc_retry      = 0;
 	pspf->pspf_fc.fc_pwr_mgt    = 1; //power management bit on
 	pspf->pspf_fc.fc_more_data  = 0;
 	pspf->pspf_fc.fc_wep        = 0;
 	pspf->pspf_fc.fc_order      = 0;

	STORE4BYTE(&(mac->index_), pspf->pspf_ra);
	STORE4BYTE(&src, pspf->pspf_ta);

	pspf->pspf_aid = e->aid;

	// recv pspoll
	target->recvPSPoll(psp);

	// 状態チェック
	CPPUNIT_ASSERT(!e->isPolled); // tx_resume で deque されるので false に戻っているはず
	CPPUNIT_ASSERT(target->sendingPUcast_);
	CPPUNIT_ASSERT(mac->pktCTRL_);  // ACK sent?
	CPPUNIT_ASSERT(mac->pktTx_ == p);
}

// enque テスト (broadcast)
void PS_AP_Test::testEnqueuePSPacket1()
{
	u_int32_t dst = MAC_BROADCAST;

	Packet *p = Packet::alloc();
	struct hdr_mac802_11* dh = HDR_MAC802_11(p);
	STORE4BYTE(&dst, dh->dh_ra);

	// create STA
	mac->update_client_table(1, 1, 1);
	struct client_table *e = mac->find_client_entry(1);
	e->aid = 2;

	CPPUNIT_ASSERT(target->bcast_queue_.length() == 0);

	// no PS-mode STA
	e->isPowerSave = false;
	int ret = target->enqueuePSPacket(p);
	CPPUNIT_ASSERT(ret == 0);
	CPPUNIT_ASSERT(target->bcast_queue_.length() == 0);

	// has PS-mode STA
	e->isPowerSave = true;
	ret = target->enqueuePSPacket(p);
	CPPUNIT_ASSERT(ret != 0);
	CPPUNIT_ASSERT(target->bcast_queue_.length() == 1);
}

// enque テスト (unicast)
void PS_AP_Test::testEnqueuePSPacket2()
{
	u_int32_t dst = 2;

	Packet *p = Packet::alloc();
	struct hdr_mac802_11* dh = HDR_MAC802_11(p);
	STORE4BYTE(&dst, dh->dh_ra);

	// create STA
	mac->update_client_table(dst, 1, 1);
	struct client_table *e = mac->find_client_entry(dst);
	e->aid = 2;

	// no PS-mode STA
	e->isPowerSave = false;
	int ret = target->enqueuePSPacket(p);
	CPPUNIT_ASSERT(ret == 0);
	CPPUNIT_ASSERT(e->pktQueue.length() == 0);

	// PS-mode STA
	e->isPowerSave = true;
	ret = target->enqueuePSPacket(p);
	CPPUNIT_ASSERT(ret != 0);
	CPPUNIT_ASSERT(e->pktQueue.length() == 1);
}

/* enque テスト3
   pktTx_ != NULL 状態 (PSパケット送信中)に呼び出すと、
   パケットがキューイングされること

   この動作は、PSパケットを送信中(pktTx_ にdequeされたパケット
   が入っている状態)で、上位層(LL)からパケット送信がなされた
   ときを指す。このパケットは、pktTx_ につなぐことができない
   ので、一時的に保管しておかなければならない。
   
   なお、Mac と LL の間ではフロー制御が行われているので、
   LL からは１個ずつしかパケットは来ない。よって、一時保管
   は１パケットのみ行えばよい(次に LL からパケットが降ってくる
   のは tx_resume() で callback を LL にあげたあととなる)
*/
void PS_AP_Test::testEnqueuePSPacket3()
{
	Packet *p = Packet::alloc();

	// pktTx == NULL のときはキューイングされないこと
	mac->pktTx_ = NULL;
	int ret = target->enqueuePSPacket(p);
	CPPUNIT_ASSERT_EQUAL(0, ret);

	// pktTx != NULL のときはキューイングされること
	mac->pktTx_ = (Packet*)0x1; // dummy
	ret = target->enqueuePSPacket(p);
	CPPUNIT_ASSERT_EQUAL(1, ret);

	// 正しく dequeue されること
	Packet *pr = target->dequeuePSPacket();
	CPPUNIT_ASSERT(pr == p);
}

// deque テスト (broadcast)
void PS_AP_Test::testDequeuePSPacket1()
{
	Packet *p = Packet::alloc();
	Packet *p2 = Packet::alloc();
	struct hdr_mac802_11 *dh;
	Packet *pr;

	target->bcast_queue_.enque(p);
	target->bcast_queue_.enque(p2);
	target->sendingPBcast_ = true;

	// deque 1st one
	pr = target->dequeuePSPacket();
	CPPUNIT_ASSERT(pr);
	CPPUNIT_ASSERT(pr == p);
	dh = HDR_MAC802_11(pr);
	CPPUNIT_ASSERT(dh->dh_fc.fc_more_data == 1);
	CPPUNIT_ASSERT(target->sendingPBcast_);

	// deque 2nd one
	pr = target->dequeuePSPacket();
	CPPUNIT_ASSERT(pr);
	CPPUNIT_ASSERT(pr == p2);
	dh = HDR_MAC802_11(pr);
	CPPUNIT_ASSERT(dh->dh_fc.fc_more_data == 0);
	CPPUNIT_ASSERT(!target->sendingPBcast_);
}


// deque テスト (unicast)
void PS_AP_Test::testDequeuePSPacket2()
{
	u_int32_t dst = 2;
	mac->update_client_table(dst+1, 1, 1); // dummy entry
	mac->update_client_table(dst, 1, 1);
	struct client_table *e = mac->find_client_entry(dst);
	e->aid = 2;
	e->isPowerSave = true;
	e->isPolled = false;

	Packet *p = Packet::alloc();
	Packet *p2 = Packet::alloc();
	struct hdr_mac802_11 *dh;
	Packet *pr;

	e->pktQueue.enque(p);
	e->pktQueue.enque(p2);
	target->sendingPUcast_ = true;

	// isPolled でなければデキューされないことをチェック
	pr = target->dequeuePSPacket();
	CPPUNIT_ASSERT(pr == NULL);
	CPPUNIT_ASSERT(!target->sendingPUcast_);
	target->sendingPUcast_ = true;

	// deque 1st one
	e->isPolled = true;
	pr = target->dequeuePSPacket();
	CPPUNIT_ASSERT(pr != NULL);
	CPPUNIT_ASSERT(pr == p);
	dh = HDR_MAC802_11(pr);
	CPPUNIT_ASSERT(dh->dh_fc.fc_more_data == 1);
	CPPUNIT_ASSERT(target->sendingPUcast_);
	CPPUNIT_ASSERT(!e->isPolled);

	// deque 2nd one
	e->isPolled = true;
	pr = target->dequeuePSPacket();
	CPPUNIT_ASSERT(pr != NULL);
	CPPUNIT_ASSERT(pr == p2);
	dh = HDR_MAC802_11(pr);
	CPPUNIT_ASSERT(dh->dh_fc.fc_more_data == 0);
	CPPUNIT_ASSERT(target->sendingPUcast_);
	CPPUNIT_ASSERT(!e->isPolled);

	// deque : no more packet
	e->isPolled = true;
	pr = target->dequeuePSPacket();
	CPPUNIT_ASSERT(pr == NULL);
	CPPUNIT_ASSERT(!target->sendingPUcast_);
	CPPUNIT_ASSERT(!e->isPolled);

}
