/* -*-  Mode:C++; c-basic-offset:8; tab-width:8; indent-tabs-mode:t -*- */
// PS_STA �ƥ���

#include <cppunit/extensions/HelperMacros.h>

#include "testdummy.h"
#include "packet.h"
#define private public
#define protected public
#include "mac.h"
#include "mac-802_11.h"
#include "mac-802_11-pssta.h"
#undef private
#undef protected
#include "wireless-phy.h"

class PS_STA_Test : public CPPUNIT_NS::TestFixture
{
	CPPUNIT_TEST_SUITE( PS_STA_Test );
	CPPUNIT_TEST(testCreate);
	CPPUNIT_TEST(testSetAid);
	CPPUNIT_TEST(testReadBeacon1);
	CPPUNIT_TEST(testReadBeacon2);
	CPPUNIT_TEST(testReadBeacon3);
	CPPUNIT_TEST(testWakeupHandler);
	CPPUNIT_TEST(testSetSleep);
	CPPUNIT_TEST(testSendPSPoll);
	CPPUNIT_TEST(testRetransmitPSPoll);
	CPPUNIT_TEST(testCheckMoreDataFlag);
	CPPUNIT_TEST_SUITE_END();

private:
	Mac802_11 *mac;
	PS_STA *target;
	WirelessPhy *phy;

public:
	void setUp();
	void tearDown();

	void testCreate();
	void testSetAid();
	void testReadBeacon1();
	void testReadBeacon2();
	void testReadBeacon3();
	void testWakeupHandler();
	void testSetSleep();
	void testSendPSPoll();
	void testRetransmitPSPoll();
	void testCheckMoreDataFlag();
};
CPPUNIT_TEST_SUITE_REGISTRATION(PS_STA_Test);

////////////////////////////////////////////////////////////////////////////

void PS_STA_Test::setUp()
{
	mac = allocMac();
	target = &mac->pssta;

	mac->index_ = 1;
	mac->phymib_.isPowerSave = true;
	mac->ScanType_ = 1;

	phy = (WirelessPhy*)mac->netif_;
	phy->node_wakeup();
}

void PS_STA_Test::tearDown()
{
	freeMac(mac);
}

void PS_STA_Test::testCreate()
{
	CPPUNIT_ASSERT_EQUAL(mac, target->mac_);
	CPPUNIT_ASSERT(target->pktPSPOLL_ == NULL);
	CPPUNIT_ASSERT_EQUAL(true, target->waitingDTIM_);
	CPPUNIT_ASSERT_EQUAL(false, target->waitingBroadcast_);
	CPPUNIT_ASSERT_EQUAL(false, target->waitingUnicast_);
}

void PS_STA_Test::testSetAid()
{
	target->set_aid(2007);
	CPPUNIT_ASSERT(target->aid_ == 2007);
}

void PS_STA_Test::testReadBeacon1()
{
	target->set_aid(30);

	struct beacon_frame bf;
	memset(&bf, 0, sizeof(bf));

	// this is TIM, no PVB
	bf.bf_dtimcount = 1;
	bf.bf_dtimperiod = 10;
	bf.bf_bitmapcontrol = 0;
	bf.bf_bcninterval = 0.1;
	bf.clear_pvb();

	target->waitingDTIM_ = true;
	target->waitingUnicast_ = true;
	target->waitingBroadcast_ = true;

	target->readBeacon(&bf);

	CPPUNIT_ASSERT(!target->waitingDTIM_);
	CPPUNIT_ASSERT(mac->mhWakeup_.busy());
	CPPUNIT_ASSERT(!target->waitingUnicast_);
	CPPUNIT_ASSERT(!target->waitingBroadcast_);

	// DTIM
	target->waitingDTIM_ = true;
	target->waitingUnicast_ = true;
	target->waitingBroadcast_ = true;

	bf.bf_dtimcount = 0;
	target->readBeacon(&bf);
	CPPUNIT_ASSERT(!target->waitingDTIM_);
	CPPUNIT_ASSERT(mac->mhWakeup_.busy());
	CPPUNIT_ASSERT(!target->waitingUnicast_);
	CPPUNIT_ASSERT(!target->waitingBroadcast_);
}

void PS_STA_Test::testReadBeacon2()
{
	target->set_aid(30);

	struct beacon_frame bf;
	memset(&bf, 0, sizeof(bf));

	// this is DTIM, has broadcast
	bf.bf_dtimcount = 0;
	bf.bf_dtimperiod = 10;
	bf.bf_bitmapcontrol = 1;
	bf.bf_bcninterval = 0.1;
	bf.clear_pvb();

	target->waitingDTIM_ = true;
	target->waitingUnicast_ = true;
	target->waitingBroadcast_ = false;

	target->readBeacon(&bf);

	CPPUNIT_ASSERT(!target->waitingDTIM_);
	CPPUNIT_ASSERT(!target->waitingUnicast_);
	CPPUNIT_ASSERT(target->waitingBroadcast_);
	CPPUNIT_ASSERT(mac->mhWakeup_.busy());
}

void PS_STA_Test::testReadBeacon3()
{
	target->set_aid(30);

	struct beacon_frame bf;
	memset(&bf, 0, sizeof(bf));

	// this is DTIM, has unicast
	bf.bf_dtimcount = 0;
	bf.bf_dtimperiod = 10;
	bf.bf_bitmapcontrol = 0;
	bf.bf_bcninterval = 0.1;
	bf.clear_pvb();
	bf.set_pvb(30);

	target->waitingDTIM_ = true;
	target->waitingUnicast_ = false;
	target->waitingBroadcast_ = true;

	target->readBeacon(&bf);

	CPPUNIT_ASSERT(!target->waitingDTIM_);
	CPPUNIT_ASSERT(target->waitingUnicast_);
	CPPUNIT_ASSERT(!target->waitingBroadcast_);
	CPPUNIT_ASSERT(mac->mhWakeup_.busy());

	CPPUNIT_ASSERT(target->pktPSPOLL_ != NULL);
}

void PS_STA_Test::testWakeupHandler()
{
	phy->node_sleep();

	CPPUNIT_ASSERT(!phy->isWakeup);

	target->waitingDTIM_ = false;
	mac->mhWakeup_.handle(NULL);

	CPPUNIT_ASSERT(phy->isWakeup);
	CPPUNIT_ASSERT_EQUAL(true, target->waitingDTIM_);
}

void PS_STA_Test::testSetSleep()
{
	WirelessPhy *phy = (WirelessPhy*)mac->netif_;
	EnergyModel *em = mac->netif_->node()->energy_model();

	target->setSleep(true);
	CPPUNIT_ASSERT(!phy->isWakeup);
	CPPUNIT_ASSERT(em->sleep());

	target->setSleep(false);
	CPPUNIT_ASSERT(phy->isWakeup);
	CPPUNIT_ASSERT(!em->sleep());

	target->setSleep(true);
	CPPUNIT_ASSERT(!phy->isWakeup);
	CPPUNIT_ASSERT(em->sleep());
}

void PS_STA_Test::testSendPSPoll()
{
	target->set_aid(30);

	target->sendPSPoll(2);

	Packet *p = target->pktPSPOLL_;
	CPPUNIT_ASSERT(p);
    
	//hdr_cmn* ch = HDR_CMN(p);
	struct pspoll_frame *pspf = (struct pspoll_frame*)p->access(hdr_mac::offset_);

	CPPUNIT_ASSERT(pspf->pspf_fc.fc_subtype == MAC_Subtype_PSPoll);
	CPPUNIT_ASSERT_EQUAL(1, GET4BYTE(pspf->pspf_ta));
	CPPUNIT_ASSERT_EQUAL(2, GET4BYTE(pspf->pspf_ra));
	CPPUNIT_ASSERT(pspf->pspf_aid == 30);

	CPPUNIT_ASSERT(mac->mhBackoff_.busy());
}

void PS_STA_Test::testRetransmitPSPoll()
{
	target->set_aid(30);
	target->sendPSPoll(2);

	mac->mhBackoff_.stop();
	
	mac->ssrc_ = 0;
	mac->cw_ = 31;
	target->RetransmitPSPoll();

	CPPUNIT_ASSERT(mac->cw_ == 63);
	CPPUNIT_ASSERT(mac->ssrc_ == 1);
	CPPUNIT_ASSERT(mac->pssta.pktPSPOLL_ != NULL);
	struct hdr_mac802_11 *dh = HDR_MAC802_11(mac->pssta.pktPSPOLL_);
	CPPUNIT_ASSERT(dh->dh_fc.fc_retry == 1);

	// retry over
	mac->mhBackoff_.stop();

	mac->ssrc_ = 30;
	target->RetransmitPSPoll();

	CPPUNIT_ASSERT(mac->ssrc_ == 0);
	CPPUNIT_ASSERT(mac->pssta.pktPSPOLL_ == NULL);
}

void PS_STA_Test::testCheckMoreDataFlag()
{
	u_int32_t dst;

	Packet *p = Packet::alloc();
	struct hdr_mac802_11 *dh = HDR_MAC802_11(p);

	target->waitingDTIM_ = true;

	// more data, broadcast
	dst = MAC_BROADCAST;
	STORE4BYTE(&dst, dh->dh_ra);
	dh->dh_fc.fc_more_data = 1;

	target->waitingBroadcast_ = true;
	target->waitingUnicast_ = true;
	target->checkMoreDataFlag(p);
	CPPUNIT_ASSERT(target->waitingBroadcast_);
	CPPUNIT_ASSERT(target->waitingUnicast_);
	CPPUNIT_ASSERT(target->pktPSPOLL_ == NULL);
	CPPUNIT_ASSERT(target->waitingDTIM_);

	// no more data, broadcast
	dh->dh_fc.fc_more_data = 0;

	target->checkMoreDataFlag(p);
	CPPUNIT_ASSERT(!target->waitingBroadcast_);
	CPPUNIT_ASSERT(target->waitingUnicast_);
	CPPUNIT_ASSERT(target->waitingDTIM_);

	// more data, unicast
	dst = 1;
	STORE4BYTE(&dst, dh->dh_ra);
	dh->dh_fc.fc_more_data = 1;

	target->waitingBroadcast_ = true;
	target->waitingUnicast_ = true;
	target->checkMoreDataFlag(p);
	CPPUNIT_ASSERT(target->waitingBroadcast_);
	CPPUNIT_ASSERT(target->waitingUnicast_);
	CPPUNIT_ASSERT(target->pktPSPOLL_ != NULL);
	CPPUNIT_ASSERT(target->waitingDTIM_);

	// no more data, unicast
	dh->dh_fc.fc_more_data = 0;

	target->checkMoreDataFlag(p);
	CPPUNIT_ASSERT(target->waitingBroadcast_);
	CPPUNIT_ASSERT(!target->waitingUnicast_);
	CPPUNIT_ASSERT(target->waitingDTIM_);
}
