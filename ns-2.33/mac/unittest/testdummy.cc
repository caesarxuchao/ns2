/* -*-  Mode:C++; c-basic-offset:8; tab-width:8; indent-tabs-mode:t -*- */
#include "testdummy.h"
#include "packet.h"
#define private public
#define protected public
#include "mac.h"
#include "mac-802_11.h"
#include "mac-802_11-psap.h"
#include "mac-802_11-pssta.h"
#undef private
#undef protected
#include "wireless-phy.h"

p_info packet_info;
Packet *Packet::free_ = 0;
int Packet::hdrlen_ = 0;

int hdr_mac::offset_;
int hdr_cmn::offset_;

Tcl Tcl::the_instance;
Scheduler Scheduler::the_scheduler;

char ** p_info::name_;
unsigned int p_info::nPkt_;

// initialize headers
static void addHeader(int *offset, int hdrlen)
{
	*offset = Packet::hdrlen_;
	Packet::hdrlen_ += hdrlen;
}

void initHeaders()
{
	addHeader(&hdr_cmn::offset_, sizeof(hdr_cmn));
	addHeader(&hdr_mac::offset_, MAC_HDR_LEN);
}

// subroutines
Mac802_11 *allocMac(void)
{
    	Mac802_11 *mac = new Mac802_11();

	mac->netif_ = new WirelessPhy();
	mac->netif_->node_ = new Node();

	mac->index_ = 0;  // my address
	mac->bugFix_timer_ = 1;
	mac->ScanType_ = 1;
	mac->infra_mode_ = 1;
	mac->ap_list = NULL;
	mac->basicRate_ = 1000000;

	PHY_MIB *p = &mac->phymib_;
	p->CWMin = 31;
	p->CWMax = 1023;
	p->SlotTime = 0.00002;
	p->SIFSTime = 0.00001;
	p->BeaconInterval = 0.1;
	p->PreambleLength = 144;
	p->PLCPHeaderLength = 48;
	p->PLCPDataRate = 1000000;
	p->isPowerSave = false;
	p->DTIMPeriod = 10;

	MAC_MIB *m = &mac->macmib_;
	m->RTSThreshold = 500;
	m->ShortRetryLimit = 6;
	m->LongRetryLimit = 6;
	m->MaxChannelTime = 0.011;
	m->MinChannelTime = 0.005;
	m->ChannelTime = 0.12;

	// create cache
	char *argv[] = {"ns", "nodes", "10"};
	mac->command(3, argv);

	return mac;
}

void freeMac(Mac802_11 *mac)
{
	delete mac->netif_->node_;
	delete mac->netif_;
	delete mac;
}

