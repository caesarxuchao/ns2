/* -*-  Mode:C++; c-basic-offset:8; tab-width:8; indent-tabs-mode:t -*- */
// Mac クラステスト (STA側)

#include <cppunit/extensions/HelperMacros.h>

#include "testdummy.h"
#include "packet.h"
#define private public
#define protected public
#include "mac.h"
#include "mac-802_11.h"
#include "mac-802_11-psap.h"
#include "mac-802_11-pssta.h"
#undef private
#undef protected
#include "wireless-phy.h"

class MacSTA_Test : public CPPUNIT_NS::TestFixture
{
	CPPUNIT_TEST_SUITE( MacSTA_Test );
	CPPUNIT_TEST(testRecvASSOCREP);
	CPPUNIT_TEST(testTxResume_sleep);
	CPPUNIT_TEST(testBackoffHandlerPSPoll);
	CPPUNIT_TEST(testRetransPSPoll);
	CPPUNIT_TEST_SUITE_END();

private:
	Mac802_11 *mac;
	PS_STA *pssta;
	WirelessPhy *phy;
	Node *node;

public:
	void setUp();
	void tearDown();

	void testRecvASSOCREP();
	void testTxResume_sleep();
	void testBackoffHandlerPSPoll();
	void testRetransPSPoll();

private:
	Packet *generatePSPoll();

};
CPPUNIT_TEST_SUITE_REGISTRATION(MacSTA_Test);

////////////////////////////////////////////////////////////////////////////

const u_int32_t BssId = 0; // node 0
const u_int32_t MyAddr = 1; // node 1

void MacSTA_Test::setUp()
{
	mac = allocMac();
	pssta = &mac->pssta;

	// PS-STA
	mac->index_ = 1;  // my address
	mac->phymib_.isPowerSave = true;
	mac->ScanType_ = 1;
}

void MacSTA_Test::tearDown()
{
	freeMac(mac);
}

// recvASSOCREP() において、正しく AID を保存していることを確認する。
void MacSTA_Test::testRecvASSOCREP()
{
	Packet *p = Packet::alloc();
	hdr_cmn* ch = HDR_CMN(p);
	struct assocrep_frame *acrpf =(struct assocrep_frame*)p->access(hdr_mac::offset_);

	ch->uid() = 0;

	ch->ptype() = PT_MAC;
	ch->size() = mac->phymib_.getASSOCREPlen();
	ch->iface() = -2;
	ch->error() = 0;
	
	bzero(acrpf, MAC_HDR_LEN);

	acrpf->acrpf_fc.fc_protocol_version = MAC_ProtocolVersion;
	acrpf->acrpf_fc.fc_type	= MAC_Type_Management;
	acrpf->acrpf_fc.fc_subtype	= MAC_Subtype_AssocRep;
	acrpf->acrpf_fc.fc_to_ds	= 0;
	acrpf->acrpf_fc.fc_from_ds	= 0;
	acrpf->acrpf_fc.fc_more_frag= 0;
	acrpf->acrpf_fc.fc_retry	= 0;
	acrpf->acrpf_fc.fc_pwr_mgt	= 0;
	acrpf->acrpf_fc.fc_more_data= 0;
	acrpf->acrpf_fc.fc_wep	= 0;
	acrpf->acrpf_fc.fc_order	= 0;
	
	STORE4BYTE(&MyAddr, (acrpf->acrpf_ra));
	STORE4BYTE(&BssId, (acrpf->acrpf_ta));
	STORE4BYTE(&BssId, (acrpf->acrpf_3a));

	acrpf->acrpf_statuscode = 0;

	// Set AID
	acrpf->acrpf_aid = 500;

	// set dummy assoc req. etc.
	mac->pktASSOCREQ_ = Packet::alloc();
	mac->pktASSOCREQ_->uid_ = 0; // ###
	mac->mhSend_.start(100);

	// call recvASSOCREP
	mac->recvASSOCREP(p);

	CPPUNIT_ASSERT(pssta->aid_ == 500);
}


// tx_resume() を呼び出したときに、sleep に入れる状態に
// なっていれば sleep に入ることを確認する
void MacSTA_Test::testTxResume_sleep()
{
	((WirelessPhy*)mac->netif_)->node_wakeup();

	pssta->waitingDTIM_ = false;
	mac->tx_resume();
    
	CPPUNIT_ASSERT(! ((WirelessPhy*)mac->netif_)->isWakeup);
}

// txHandler() において、sleep 可能チェックが行われる
// ことを確認する

// backoffHandler() を呼び出したときに、PSPoll パケットが
// キューイングされていれば送信が開始されることを確認する
void MacSTA_Test::testBackoffHandlerPSPoll()
{
	Packet *p = generatePSPoll();

	// call backoffHandler
	mac->pssta.pktPSPOLL_ = p;
	mac->backoffHandler();

	// check
	CPPUNIT_ASSERT(mac->tx_state_ == MAC_PSP);
}

Packet *MacSTA_Test::generatePSPoll()
{
	Packet *p = Packet::alloc();
        struct pspoll_frame *pspf = (struct pspoll_frame*)p->access(hdr_mac::offset_);
        bzero(pspf, MAC_HDR_LEN);

        pspf->pspf_fc.fc_protocol_version = MAC_ProtocolVersion;
        pspf->pspf_fc.fc_type       = MAC_Type_Control;
        pspf->pspf_fc.fc_subtype    = MAC_Subtype_PSPoll;
        pspf->pspf_fc.fc_to_ds      = 0;
        pspf->pspf_fc.fc_from_ds    = 0;
        pspf->pspf_fc.fc_more_frag  = 0;
        pspf->pspf_fc.fc_retry      = 0;
        pspf->pspf_fc.fc_pwr_mgt    = 1; //power management bit on
        pspf->pspf_fc.fc_more_data  = 0;
        pspf->pspf_fc.fc_wep        = 0;
        pspf->pspf_fc.fc_order      = 0;

        //STORE4BYTE(&dst, (pspf->pspf_ra));
	STORE4BYTE(&mac->index_, pspf->pspf_ta);

	return p;
}

// send_timer() において、PS-Poll の再送処理が開始される
// ことを確認する。
void MacSTA_Test::testRetransPSPoll()
{
	Packet *p = generatePSPoll();

	mac->pssta.pktPSPOLL_ = p;
	mac->tx_state_ = MAC_PSP;
	mac->cw_ = 31;

	// call send_timer
	mac->sendHandler();

	// check CW
	CPPUNIT_ASSERT(mac->cw_ == 63);
}


// recvDATA() において、Power Save STA では More Data ビット
// を確認し、適切な処理を行うことを確認する。

// recvACK() において、PS-Poll の再送タイマが停止されることを
// 確認する。
