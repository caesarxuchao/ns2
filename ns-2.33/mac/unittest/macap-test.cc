/* -*-  Mode:C++; c-basic-offset:8; tab-width:8; indent-tabs-mode:t -*- */
// Mac クラステスト (AP側)

#include <cppunit/extensions/HelperMacros.h>

#include "testdummy.h"
#include "packet.h"
#define private public
#define protected public
#include "mac.h"
#include "mac-802_11.h"
#include "mac-802_11-psap.h"
#include "mac-802_11-pssta.h"
#include "wireless-phy.h"
#undef private
#undef protected

class MacAP_Test : public CPPUNIT_NS::TestFixture
{
	CPPUNIT_TEST_SUITE( MacAP_Test );
	CPPUNIT_TEST(testTxResume_sendQPacket);
	CPPUNIT_TEST(testDiscardPSPoll);
	CPPUNIT_TEST(testSendEnque);
	CPPUNIT_TEST(testRecvPwrMgt);
	CPPUNIT_TEST_SUITE_END();

private:
	Mac802_11 *mac;
	WirelessPhy *phy;
	Node *node;

public:
	void setUp();
	void tearDown();

	void testTxResume_sendQPacket();
	void testDiscardPSPoll();
	void testSendEnque();
	void testRecvPwrMgt();
};
CPPUNIT_TEST_SUITE_REGISTRATION(MacAP_Test);

////////////////////////////////////////////////////////////////////////////

void MacAP_Test::setUp()
{
	mac = allocMac();

	// AP
	mac->index_ = 0;
	mac->ap_addr = 0;
	mac->bss_id_ = 0;
	mac->infra_mode_ = 1;
}

void MacAP_Test::tearDown()
{
	freeMac(mac);
}

// PS-Poll が正しく discard されることを確認する
void MacAP_Test::testDiscardPSPoll()
{
	Packet *p = Packet::alloc();
       	struct pspoll_frame *pspf = (struct pspoll_frame*)p->access(hdr_mac::offset_);
		
  	pspf->pspf_fc.fc_protocol_version = MAC_ProtocolVersion;
	pspf->pspf_fc.fc_type       = MAC_Type_Control;
 	pspf->pspf_fc.fc_subtype    = MAC_Subtype_PSPoll;
 	pspf->pspf_fc.fc_to_ds      = 0;
 	pspf->pspf_fc.fc_from_ds    = 0;
 	pspf->pspf_fc.fc_more_frag  = 0;
 	pspf->pspf_fc.fc_retry      = 0;
 	pspf->pspf_fc.fc_pwr_mgt    = 1; //power management bit on
 	pspf->pspf_fc.fc_more_data  = 0;
 	pspf->pspf_fc.fc_wep        = 0;
 	pspf->pspf_fc.fc_order      = 0;

	STORE4BYTE(&mac->index_, pspf->pspf_ra);

	mac->discard(p, "reason?");
}

// tx_resume() を呼び出したときに、PS送信待ちパケットを
// 取り出して送信することを確認する
void MacAP_Test::testTxResume_sendQPacket()
{
	// queue broadcast packet
	Packet *p = Packet::alloc();
	mac->psap.bcast_queue_.enque(p);
	mac->psap.sendingPBcast_ = true;

	mac->tx_resume();

	// check
	CPPUNIT_ASSERT(mac->pktTx_ == p);
	CPPUNIT_ASSERT(mac->mhBackoff_.busy());
}

// PS送信待ちパケットが pktTx_ に入った状態で、上位層から
// パケットの送信が行われると、このパケットが別途キューイングされること。
// PS送信待ちパケットの送信が終わると、上位パケットがデキューされ、
// 送信処理が行われ、これが完了すると上位層のコールバックが呼ばれること。
#if 0
void MacAP_Test::testEnqueOtherPacket()
{
	
}
#endif



// send() において、AP ではパケットのエンキューが行われる
// ことを確認する。
void MacAP_Test::testSendEnque()
{
	u_int32_t dst = MAC_BROADCAST;

	// PS STA 生成
	mac->update_client_table(1, 1, 1);
	struct client_table *e = mac->find_client_entry(1);
	e->isPowerSave = true;

	// make broadcast
	Packet *p = Packet::alloc();
	struct hdr_mac802_11 *dh = HDR_MAC802_11(p);
	STORE4BYTE(&dst, dh->dh_ra);

	mac->send(p, (Handler*)0x1);
	
	CPPUNIT_ASSERT(mac->psap.bcast_queue_.length() == 1);
	CPPUNIT_ASSERT(mac->callback_ == (Handler*)0x1);
}


// recv_timer() において、PS-Poll パケットを受信したときに、
// PS_AP::recvPSPoll が呼ばれることを確認する。

// sendBEACON() において、正しくビーコンが生成されることを確認する。

// sendASSOCREP() において、正しく AID が設定されることを確認する。

// フレーム受信において、正しく power management bit が確認されることを
// 確認する。
void MacAP_Test::testRecvPwrMgt()
{
	u_int32_t dst = MAC_BROADCAST;
	u_int32_t src = 1;

	// STA 生成
	mac->update_client_table(src, 1, 1);
	struct client_table *e = mac->find_client_entry(src);
	CPPUNIT_ASSERT(!e->isPowerSave);

	// ダミーパケットを生成
	Packet *p = Packet::alloc();
	struct hdr_mac802_11 *dh = HDR_MAC802_11(p);
	dh->dh_fc.fc_protocol_version = MAC_ProtocolVersion;
	dh->dh_fc.fc_type = MAC_Type_Data;
	dh->dh_fc.fc_subtype = MAC_Subtype_Data;
	dh->dh_fc.fc_to_ds = 1;
	dh->dh_fc.fc_from_ds = 0;
	dh->dh_fc.fc_more_frag = 0;
	dh->dh_fc.fc_retry = 0;
	dh->dh_fc.fc_pwr_mgt = 1; 
	dh->dh_fc.fc_more_data = 0;
	dh->dh_fc.fc_wep = 0;
	dh->dh_fc.fc_order = 0;

	STORE4BYTE(&mac->index_, dh->dh_ra); //BSSID
	STORE4BYTE(&src, dh->dh_ta); //SA
	STORE4BYTE(&dst, dh->dh_3a); //DA

	// 受信前状態を作る
	mac->pktRx_ = p;
	mac->rx_state_ = MAC_RECV;

	// recvHandler 呼び出し
	mac->recvHandler();

	// チェック
	CPPUNIT_ASSERT(e->isPowerSave);
}
