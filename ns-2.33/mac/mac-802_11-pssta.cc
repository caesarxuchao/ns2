/* -*-	Mode:C++; c-basic-offset:8; tab-width:8; indent-tabs-mode:t -*-
 *
 * Copyright (C) 2008
 * System Platforms Research Laboratories, NEC Corporation.
 * All rights reserved.
 *
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License,
 * version 2, as published by the Free Software Foundation.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License along
 * with this program; if not, write to the Free Software Foundation, Inc.,
 * 59 Temple Place, Suite 330, Boston, MA 02111-1307, USA.
 *
 *
 * The copyright of this module includes the following
 * linking-with-specific-other-licenses addition:
 *
 * In addition, as a special exception, the copyright holders of
 * this module give you permission to combine (via static or
 * dynamic linking) this module with free software programs or
 * libraries that are released under the GNU LGPL and with code
 * included in the standard release of ns-2 under the Apache 2.0
 * license or under otherwise-compatible licenses with advertising
 * requirements (or modified versions of such code, with unchanged
 * license).  You may copy and distribute such a system following the
 * terms of the GNU GPL for this module and the licenses of the
 * other code concerned, provided that you include the source code of
 * that other code when and as the GNU GPL requires distribution of
 * source code.
 *
 * Note that people who make modified versions of this module
 * are not obligated to grant this special exception for their
 * modified versions; it is their choice whether to do so.  The GNU
 * General Public License gives permission to release a modified
 * version without this exception; this exception also makes it
 * possible to release a modified version which carries forward this
 * exception.
 *
 */

/*
  802.11 Power Management Extension
    STA side implementation
*/

#include "packet.h"
#include "random.h"

#include "mac.h"
#include "mac-timers.h"
#include "mac-802_11-pssta.h"
#include "mac-802_11.h"
#include "cmu-trace.h"

#undef RECV  // ad hoc...
#undef SEND  // ad hoc...
#include "wireless-phy.h"
#include "ll.h"




/* PMExt fujinami */

PS_STA::PS_STA(Mac802_11 *m)
{
	//initialization
	pktPSPOLL_ = NULL;
	waitingDTIM_ = true;
	waitingBroadcast_ = false;
	waitingUnicast_ = false;
	
	mac_ = m;
}


void PS_STA::setSleep(bool isSleep)
{
	//CHAO: I commented this out, because even for non-PS node,
	//it should sleep during NoA to save power.
//	if(!mac_->phymib_.getisPowerSave()){
//		return; // do nothing
//	}

	if(!isSleep){
		// wakeup
		mac_->pme_trace("wakeup: %d\n", mac_->index_);
		((WirelessPhy *)mac_->netif_)->node_wakeup();
	} else {          
		// sleep
		mac_->pme_trace("sleep: %d\n", mac_->index_);
		((WirelessPhy *)mac_->netif_)->node_sleep();

	}

	EnergyModel *em = mac_->netif_->node()->energy_model();
	if (em) {
		if(!isSleep && em->sleep()){
			em->set_node_sleep(0);
			em->set_node_state(EnergyModel::INROUTE);
		} else if(isSleep && !em->sleep()) {          
			em->set_node_sleep(1);
			em->set_node_state(EnergyModel::POWERSAVING);
		}
	}
}

void PS_STA::wakeupHandler()
{
	waitingDTIM_ = true;
	checkSleep();
}

//CHAO: WFD PM
void PS_STA::STANoAWaitHandler()
{
	//only non-PS node may trigger this handler
	if (mac_->phymib_.getisPowerSave())
		return;

	assert(mac_->tx_state_ == MAC_IDLE);
	if (mac_->mhBackoff_.busy()) {
		mac_->mhBackoff_.stop();
	}
	if (mac_->mhDefer_.busy()) {
		mac_->mhDefer_.stop();
	}
//	save_waitingBroadcast_ = waitingBroadcast_;
//	save_waitingDTIM_ = waitingDTIM_;
//	save_waitingUnicast_ = waitingUnicast_;
	waitingBroadcast_ = 0;
	waitingDTIM_ = 0;
	waitingUnicast_ = 0;

	mac_->mhNoAAbsent.start(mac_->_NoA_Duration);
	setSleep(true);
}

void PS_STA::STANoAAbsentHandler()
{
	//only non-PS node may trigger this handler
	if (mac_->phymib_.getisPowerSave())
		return;
	setSleep(false);
	mac_->_NoA_count--;
	mac_->tx_resume();
	if (mac_->_NoA_count==0)
		return;
	mac_->mhNoAWait.start((double)(mac_->_NoA_Interval));

}

void PS_STA::STACTWindowHandler()
{
	//CHAO: this handler is NULL.
}

void PS_STA::checkSleep()
{
	if (mac_->addr() != mac_->bss_id_ && mac_->phymib_.getisPowerSave()) {
		if (mac_->tx_state_ == MAC_IDLE &&
		    !waitingDTIM_ && 
		    !waitingBroadcast_ && 
		    !waitingUnicast_ && 
		    !mac_->mhBackoff_.busy() && 
		    !mac_->mhDefer_.busy())
		{

			setSleep(true);
		}else{

			setSleep(false);
		}
	}
}

int PS_STA::check_pktPSPOLL()
{
	mac_->pme_trace("check_pktPSPOLL : index_= %d\n", mac_->index_);

	if (pktPSPOLL_ == NULL) {
		return -1;
	}

	assert(mac_->mhBackoff_.busy() == 0);

	struct hdr_mac802_11 *mh = HDR_MAC802_11(pktPSPOLL_);

	if (mh->dh_fc.fc_subtype != MAC_Subtype_PSPoll) {
		fprintf(stderr, "check_pktPSPOLL:Invalid MAC Control subtype\n");
		exit(1);
	}

	if (!mac_->is_idle()) {
		mac_->inc_cw();
		mac_->mhBackoff_.start(mac_->cw_, mac_->is_idle());
		return 0;
	}

	mac_->setTxState(MAC_PSP); 
	double timeout = mac_->txtime(mac_->phymib_.getPSPOLLlen(), mac_->basicRate_)
		//timeout calculation should to be considered later.
//			+ macmib_.getMaxChannelTime()
		+ DSSS_MaxPropagationDelay
		+ mac_->phymib_.getSIFS()
		+ mac_->txtime(mac_->phymib_.getACKlen(), mac_->basicRate_)
		+ DSSS_MaxPropagationDelay;

	mac_->transmit(pktPSPOLL_, timeout);

	return 0;
}

void PS_STA::checkMoreDataFlag(Packet *datapacket)
{
	u_int32_t dst, src;
	struct hdr_mac802_11 *dh = HDR_MAC802_11(datapacket);
	dst = ETHER_ADDR(dh->dh_ra);
        src = ETHER_ADDR(dh->dh_ta);
	
	if(dh->dh_fc.fc_more_data == 1){
		if(dst == MAC_BROADCAST){
			waitingBroadcast_ = true;
		}else{

			/*CHAO: if we are out of CTWindow, we can't send more PSPoll to
			 * AP.
			 */
			if (mac_->_CTWindow>0 && !mac_->mhCTWindow.busy()) {
				waitingUnicast_ = false;
				checkSleep();
			} else {
				waitingUnicast_ = true;
				sendPSPoll((int)src);
			}
		}
	}else{
		if(dst == MAC_BROADCAST){ // no more data and this packet is broadcast
			waitingBroadcast_ = false;
			checkSleep();    // so I might be able to sleep now.
		}else {
			waitingUnicast_ =  false;
			//CHAO: another bug in PM Ext??? if received packets indicates
			//there is no more packet, then sta should go to sleep.
			checkSleep();
		}
	}
}

/*CHAO: 1.According to 802.11 Standard, STA only listens to beacon after
 * every ListenInterval, but the PM EXT seems to assume the ListenInterval
 * is 1. Because the readBeacon function is called every time a beacon
 * arrives, without checking LisenInterval. 2. When checking beacon, PM EXT
 * forget to set the power mode of STA to awake. Only under 2 situations
 * STA is set to awake: a. send a packet b. mhWakeup_ expires. But mhWakeup_
 * is set to DTIM interval, not LisentInterval. So the energy accounting is
 * false. A workaround is set the DTIM to be 1...
 */
void PS_STA::readBeacon(struct beacon_frame *bf, bool isWFD)
{

	int       DtimPeriod = bf->bf_dtimperiod;
	int       DtimCount = bf->bf_dtimcount;
	double    BeaconInterval = bf->bf_bcninterval;

	if(mac_->mhWakeup_.busy()){
		mac_->mhWakeup_.stop();
	}
	if(bf->bf_dtimcount == 0){
		mac_->mhWakeup_.start(DtimPeriod * BeaconInterval - 0.01);
	}else{
		mac_->mhWakeup_.start(DtimCount * BeaconInterval - 0.01);
	}
	waitingDTIM_ = false;

	if((bf->bf_bitmapcontrol & 1)){
		waitingBroadcast_ = true;
	}else{
		waitingBroadcast_ = false;
	}

 	u_int32_t src = ETHER_ADDR(bf->bf_ta);

	if(bf->is_pvb_set((int)aid_)){
		waitingUnicast_ = true;
		sendPSPoll((int)src);
		checkSleep();
	}else{
		waitingUnicast_ = false;
		/*CHAO: this seems to be a bug in PM EXT. If beacon indicates there
		 * is no packet for STA, STA should go to sleep
		 */
		checkSleep();
	}
}

//The function below will be called by recvBeacon(TIM) & recvData(Moredeata) in mac-802_11.cc
void PS_STA::sendPSPoll(int dst)
{
	mac_->pme_trace("sendPSPoll : index_= %d\n", mac_->index_);

        Packet *p = Packet::alloc();
        hdr_cmn* ch = HDR_CMN(p);

        struct pspoll_frame *pspf = (struct pspoll_frame*)p->access(hdr_mac::offset_);


	if (pktPSPOLL_) return; 
	
        ch->uid() = 0;
        ch->ptype() = PT_MAC;
        ch->size() = mac_->phymib_.getPSPOLLlen();
        ch->iface() = -2;
        ch->error() = 0;

        bzero(pspf, MAC_HDR_LEN);

        pspf->pspf_fc.fc_protocol_version = MAC_ProtocolVersion;
        pspf->pspf_fc.fc_type       = MAC_Type_Control;
        pspf->pspf_fc.fc_subtype    = MAC_Subtype_PSPoll;
        pspf->pspf_fc.fc_to_ds      = 0;
        pspf->pspf_fc.fc_from_ds    = 0;
        pspf->pspf_fc.fc_more_frag  = 0;
        pspf->pspf_fc.fc_retry      = 0;
        pspf->pspf_fc.fc_pwr_mgt    = 1; //power management bit on
        pspf->pspf_fc.fc_more_data  = 0;
        pspf->pspf_fc.fc_wep        = 0;
        pspf->pspf_fc.fc_order      = 0;

        STORE4BYTE(&dst, (pspf->pspf_ra));
	STORE4BYTE(&(mac_->index_), (pspf->pspf_ta));

	//AID
	pspf->pspf_aid = aid_;
	
        ch->next_hop() = dst; 

        pktPSPOLL_ = p;

	mac_->queueBackoff();

}

void PS_STA::RetransmitPSPoll()
{
        struct hdr_cmn *ch;
        struct hdr_mac802_11 *mh;
        u_int32_t *rcount, thresh;
        assert(mac_->mhBackoff_.busy() == 0);
        assert(pktPSPOLL_);

        ch = HDR_CMN(pktPSPOLL_);
        mh = HDR_MAC802_11(pktPSPOLL_);

        mac_->macmib_.ACKFailureCount++;

        rcount = &(mac_->ssrc_);
	thresh = mac_->macmib_.getShortRetryLimit();

        (*rcount)++;


        if(*rcount >= thresh) {
                mac_->macmib_.FailedCount++;
                /* tell the callback the send operation failed
                   before discarding the packet */
                hdr_cmn *ch = HDR_CMN(pktPSPOLL_);
                if (ch->xmit_failure_) {
                        ch->size() -= mac_->phymib_.getPSPOLLlen();
                        ch->xmit_reason_ = XMIT_REASON_ACK;
                        ch->xmit_failure_(pktPSPOLL_->copy(),
                                          ch->xmit_failure_data_);
                }

                mac_->discard(pktPSPOLL_, DROP_MAC_RETRY_COUNT_EXCEEDED);
                pktPSPOLL_ = 0;
                *rcount = 0;
                mac_->rst_cw();
        }
        else {
                struct hdr_mac802_11 *dh;
                dh = HDR_MAC802_11(pktPSPOLL_);
                dh->dh_fc.fc_retry = 1;

                mac_->inc_cw();
                mac_->mhBackoff_.start(mac_->cw_, mac_->is_idle());
        }

}

void PS_STA::StopRetransmitPSPoll()
{
        mac_->mhSend_.stop();
        mac_->rst_cw();

	Packet::free(pktPSPOLL_);
        pktPSPOLL_ = NULL;
}


void PS_STA::sendNullData()
{
         Packet *p = Packet::alloc();

         struct hdr_mac802_11* dh = HDR_MAC802_11(p);	 

	 mac_->hdr_type((char*)dh, ETHERTYPE_NULLDATA);


	 STORE4BYTE(&(mac_->index_), (dh->dh_ta));
	 STORE4BYTE(&(mac_->bss_id_), (dh->dh_ra));
	 STORE4BYTE(&(mac_->bss_id_), (dh->dh_3a));

	 mac_->sendDATA(p);


}

bool PS_STA::ispktPSPOLL_()
{
	return pktPSPOLL_ != NULL;
}

//
// Dequeue packet for PS-mode STA
//
Packet *PS_STA::dequeuePSPacket()
{
	//if it's out of ctwindow, we shouldn't dequeue anything out
	if (mac_->macmib_.getCTWindow()>0 && !mac_->mhCTWindow.busy())
		return NULL;

	return packetQueue.deque();
}

//
// return value : 0 - no need to enqueue the packet
//                1 - the packet is queued
//
void PS_STA::enqueuePSPacket(Packet *p)
{
	packetQueue.enque(p);

}

bool PS_STA::isQueueEmpty()
{
	return packetQueue.length()==0?true:false;
}
