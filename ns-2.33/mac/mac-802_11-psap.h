/* -*-	Mode:C++; c-basic-offset:8; tab-width:8; indent-tabs-mode:t -*-

 *
 * Copyright (C) 2008
 * System Platforms Research Laboratories, NEC Corporation.
 * All rights reserved.
 *
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License,
 * version 2, as published by the Free Software Foundation.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License along
 * with this program; if not, write to the Free Software Foundation, Inc.,
 * 59 Temple Place, Suite 330, Boston, MA 02111-1307, USA.
 *
 *
 * The copyright of this module includes the following
 * linking-with-specific-other-licenses addition:
 *
 * In addition, as a special exception, the copyright holders of
 * this module give you permission to combine (via static or
 * dynamic linking) this module with free software programs or
 * libraries that are released under the GNU LGPL and with code
 * included in the standard release of ns-2 under the Apache 2.0
 * license or under otherwise-compatible licenses with advertising
 * requirements (or modified versions of such code, with unchanged
 * license).  You may copy and distribute such a system following the
 * terms of the GNU GPL for this module and the licenses of the
 * other code concerned, provided that you include the source code of
 * that other code when and as the GNU GPL requires distribution of
 * source code.
 *
 * Note that people who make modified versions of this module
 * are not obligated to grant this special exception for their
 * modified versions; it is their choice whether to do so.  The GNU
 * General Public License gives permission to release a modified
 * version without this exception; this exception also makes it
 * possible to release a modified version which carries forward this
 * exception.
 *
 */

/*
  802.11 Power Management Extension
    STA side declaration
*/

#ifndef ns_mac_80211_psap_h
#define ns_mac_80211_psap_h

class Mac802_11;
struct pspoll_frame;

class PS_AP {
public:
		
	PS_AP(class Mac802_11 *);

	u_int16_t allocateAid(u_int32_t dst);
	void setupBeacon(struct beacon_frame *);
	void recvPSPoll(Packet *p);
        int enqueuePSPacket(Packet *p);
        Packet *dequeuePSPacket();
	void updatePSmode(struct frame_control &fc, u_int32_t src);

	//CHAO: WFD PM
	void APNoAWaitHandler(void);
	void APNoAAbsentHandler(void);
	void APCTWindowHandler(void);
	void setAPSleep(bool);
private:

	u_int16_t       aid_next_;	// AID counter
	u_int32_t       DTIMcount_;	// DTIM count

	PacketQueue    bcast_queue_;	// Broadcast packet queue
	bool           sendingPBcast_;	// Is sending broadcast?
	bool           sendingPUcast_;	// Is sending unicast?
	Mac802_11      *mac_;		// Back pointer to Mac class
	Packet         *pktTx_save_;	// Saved packet to be sent

	//CHAO: track whether ap is sleeping
	bool 		   isAPSleep;
};

#endif	
