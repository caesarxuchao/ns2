/* -*-	Mode:C++; c-basic-offset:8; tab-width:8; indent-tabs-mode:t -*-
 *
 * Copyright (C) 2008
 * System Platforms Research Laboratories, NEC Corporation.
 * All rights reserved.
 *
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License,
 * version 2, as published by the Free Software Foundation.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License along
 * with this program; if not, write to the Free Software Foundation, Inc.,
 * 59 Temple Place, Suite 330, Boston, MA 02111-1307, USA.
 *
 *
 * The copyright of this module includes the following
 * linking-with-specific-other-licenses addition:
 *
 * In addition, as a special exception, the copyright holders of
 * this module give you permission to combine (via static or
 * dynamic linking) this module with free software programs or
 * libraries that are released under the GNU LGPL and with code
 * included in the standard release of ns-2 under the Apache 2.0
 * license or under otherwise-compatible licenses with advertising
 * requirements (or modified versions of such code, with unchanged
 * license).  You may copy and distribute such a system following the
 * terms of the GNU GPL for this module and the licenses of the
 * other code concerned, provided that you include the source code of
 * that other code when and as the GNU GPL requires distribution of
 * source code.
 *
 * Note that people who make modified versions of this module
 * are not obligated to grant this special exception for their
 * modified versions; it is their choice whether to do so.  The GNU
 * General Public License gives permission to release a modified
 * version without this exception; this exception also makes it
 * possible to release a modified version which carries forward this
 * exception.
 *
 */

/*
  802.11 Power Management Extension
    AP side implementation
*/

#include "packet.h"
#include "random.h"

#include "mac.h"
#include "mac-timers.h"
#include "mac-802_11.h"
#include "mac-802_11-psap.h"
#include "cmu-trace.h"

#undef RECV  // ad hoc...
#undef SEND  // ad hoc...
#include "wireless-phy.h"
#include "mac-timers.h"
#include "packet.h"
#include "mac.h"
#include "queue.h"

// Constructor
PS_AP::PS_AP(Mac802_11 *m)
{
	mac_ = m;
	aid_next_ = 1;
	DTIMcount_ = mac_->phymib_.getDTIMPeriod() - 1;
	sendingPBcast_ = false;
	sendingPUcast_ = false;
	pktTx_save_ = NULL;
	isAPSleep = false;
}

//
// Allocate Association ID (AID)
//
u_int16_t PS_AP::allocateAid(u_int32_t dst)
{
	u_int16_t new_aid = aid_next_; // new_aid is AID num for current STA
	struct client_table* e;

	e = mac_->find_client_entry(dst);
       	if (e != NULL) {
		e->aid = new_aid;      
	}

	aid_next_++;
       	if (aid_next_ > MAX_AID) {
		aid_next_ = 1;
	}
	
	return new_aid;
}

//
// Update PS-mode from power management bit in a frame
//
void PS_AP::updatePSmode(struct frame_control &fc, u_int32_t src)
{
	client_table *e;
	e = mac_->find_client_entry(src);
	if (e != NULL) {
		e->isPowerSave = fc.fc_pwr_mgt;
	}
}

// Enqueue packet for PS-mode STA
// 
// return value : 0 - no need to enqueue the packet
//                1 - the packet is queued
//
int PS_AP::enqueuePSPacket(Packet *p)
{
	struct client_table *e;
	struct hdr_mac802_11 *mh = HDR_MAC802_11(p);
	
	//CHAO added this
	hdr_cmn* ch = HDR_CMN(p);
	double ackDuration = mac_->txtime(ch->size(), mac_->basicRate_);

	if((u_int32_t) ETHER_ADDR(mh->dh_ra) == MAC_BROADCAST){
		// If any STA in PS-mode exists,
		// broadcast packet must be queued.

		//CHAO: if the queue is not empty, or if NoA is imminent or ongoing,
		//we enqueue the packet
		if (bcast_queue_.length()!=0 || mac_->mhNoAAbsent.busy() ||
				(mac_->mhNoAWait.busy() && mac_->mhNoAWait.expire()-mac_->txtime(p)-ackDuration-mac_->phymib_.getSIFS()<0))
		{
			bcast_queue_.enque(p);
			return 1;
		}

		for(e = mac_->client_list; e; e = e->next){

			if(e->isPowerSave){
				bcast_queue_.enque(p);
				return 1;
			}
		}
	}
	else{
		// If designated STA is in PS-mode,
		// unicast packet must be queued.
		e = mac_->find_client_entry((u_int32_t) ETHER_ADDR(mh->dh_ra));

		//CHAO: if the queue is not empty, enqueue the packet to reserve order
		// or if NoA is ongoing or imminent, enqueue the packet
//		if (e->pktQueue.length()!=0 || mac_->mhNoAAbsent.busy() ||
//				(mac_->mhNoAWait.busy() && mac_->mhNoAWait.expire()-mac_->txtime(p)-ackDuration-mac_->phymib_.getSIFS()<0))
		if (e->pktQueue.length()!=0)
		{
			e->pktQueue.enque(p);
			return 1;
		}
		if((e && e->isPowerSave)){
			e->pktQueue.enque(p);
			return 1;
		}
		
	}

	// If some other packet is in sending state,
	// this packet must be queued.
	if(mac_->pktTx_ != NULL){
		pktTx_save_ = p;
		return 1;
	}

	// Otherwise, no need to be queued.
	return 0;
}

//
// Dequeue packet for PS-mode AP
//
Packet *PS_AP::dequeuePSPacket()
{
	Packet *p; 
	client_table *e;

	//CHAO: if AP is sleeping, it shouldn't dequeue packet out
	if (isAPSleep == true) {
		mac_->pme_trace("CHAO: AP is sleeping, so don't dequeue anything\n");
		return NULL;
	}

	/* check broadcast queue */ 
	if (sendingPBcast_) {
		p = bcast_queue_.deque();
		if(p != NULL){
			mac_->pme_trace("dequePSPacket: broadcast\n");
			struct hdr_mac802_11* dh = HDR_MAC802_11(p);		
			if(bcast_queue_.length() >= 1){
				dh->dh_fc.fc_more_data  = 1;
			}
			else{
				dh->dh_fc.fc_more_data  = 0;
				sendingPBcast_ = false;
			}
			return p;
		}

		sendingPBcast_ = false;
	}

	/* check unicast queue */
	if (sendingPUcast_) {
		for (e = mac_->client_list; e != NULL; e = e->next) {
			if (e->isPolled) {
				e->isPolled = false;
				p = e->pktQueue.deque();
				if (!p) continue;

				struct hdr_mac802_11* dh = HDR_MAC802_11(p);
				if(e->pktQueue.length() >= 1){
					dh->dh_fc.fc_more_data  = 1;
				}
				else{
					dh->dh_fc.fc_more_data  = 0;
				}
				return p;
			}
		}
		sendingPUcast_ = false;
		/*CHAO: this means we have served all pending pspoll. So, if we are
		 * already out of CTWindow, then AP should sleep now.
		 */
		if (mac_->macmib_.getCTWindow()>0 && !mac_->mhCTWindow.busy()) {
			setAPSleep(true);
		}
	}

	/* check pktTx_save_ */
	if(pktTx_save_){
		p = pktTx_save_;
		pktTx_save_ = NULL;
		return p;
	}

	return NULL;	
}

//
// Setup beacon to be sent
//
void PS_AP::setupBeacon(struct beacon_frame *bf)
{
      	// set DTIM count
	bf->bf_dtimcount = DTIMcount_;
	if(DTIMcount_ == 0){
		DTIMcount_ = mac_->phymib_.getDTIMPeriod() - 1;
	}
	else{
		DTIMcount_--;
	}
	
	// set DTIM period 
	bf->bf_dtimperiod = mac_->phymib_.getDTIMPeriod();

	// set PVB
	struct client_table* e;
	bf->clear_pvb();
	for (e = mac_->client_list; e; e = e->next) {
                if (e->isPowerSave && e->pktQueue.length() >= 1) {
                        bf->set_pvb(e->aid);
		}
	}
	
	// set bitmap control bit
	if(bcast_queue_.length() >= 1){
		bf->bf_bitmapcontrol = 1;
		sendingPBcast_ = true;
		mac_->pme_trace("sendingPBcast_ = true\n");
	}
	else{
		bf->bf_bitmapcontrol = 0;
	}
}

//
// Recv PS-Poll frame
//
void PS_AP::recvPSPoll(Packet *p)
{
	struct pspoll_frame *pf = (struct pspoll_frame*)p->access(hdr_mac::offset_);

	// check for collision
        if(mac_->tx_state_ != MAC_IDLE) {
                mac_->discard(p, DROP_MAC_BUSY);
                return;
        }

        u_int16_t aid;
	aid = (pf->pspf_aid);
        mac_->infra_mode_ = 1;
	
	// Lookup AID in the client table.
	// If it is found, mark the entry to start send.
	client_table *e;
	for(e = mac_->client_list ; e; e = e->next){
		if (e->aid == aid){
			e->isPolled = true;
			sendingPUcast_ = true;
			break;
		}
	}
	
	//send ACK to the STA
	struct hdr_mac802_11 *mh = HDR_MAC802_11(p);
	mac_->sendACK((u_int32_t) ETHER_ADDR(mh->dh_ta));

	mac_->tx_resume();

	mac_->mac_log(p);
}

//CHAO: WFD PM
void PS_AP::APNoAWaitHandler()
{
	assert(mac_->tx_state_ == MAC_IDLE);
	if (mac_->mhBackoff_.busy()) {
		mac_->mhBackoff_.stop();
	}
	if (mac_->mhDefer_.busy()) {
		mac_->mhDefer_.stop();
	}
	mac_->mhNoAAbsent.start(mac_->macmib_.getAPNoADuration());
	setAPSleep(true);
}


void PS_AP::APNoAAbsentHandler()
{
	setAPSleep(false);
	mac_->macmib_.decAPNoACount();
	mac_->tx_resume();
	if (mac_->macmib_.getAPNoACount()==0)
		return;
	mac_->mhNoAWait.start(mac_->macmib_.getAPNoAInterval());

}

void PS_AP::APCTWindowHandler()
{
	client_table *e;
	//CHAO: if there is any pending pspoll, then AP can't sleep now.
	if (sendingPUcast_) {
		return;
	}
	setAPSleep(true);
}

void PS_AP::setAPSleep(bool isSleep)
{
	if(!isSleep){
		// wakeup
		isAPSleep = false;
		mac_->pme_trace("wakeup: %d\n", mac_->index_);
		((WirelessPhy *)mac_->netif_)->node_wakeup();
	} else {
		// sleep
		isAPSleep = true;
		mac_->pme_trace("sleep: %d\n", mac_->index_);
		((WirelessPhy *)mac_->netif_)->node_sleep();

	}

	EnergyModel *em = mac_->netif_->node()->energy_model();
	if (em) {
		if(!isSleep && em->sleep()){
			em->set_node_sleep(0);
			em->set_node_state(EnergyModel::INROUTE);
		} else if(isSleep && !em->sleep()) {
			em->set_node_sleep(1);
			em->set_node_state(EnergyModel::POWERSAVING);
		}
	}
}
