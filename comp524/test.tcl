# Wireless LAN Power Management Extension : sample script
# the example provided with the patch
# node 0: GO
# 1 -> 2    512 @ 256Kb
# wifip2p_1.tcl

set val(chan)           Channel/WirelessChannel    ;#Channel Type
set val(prop)           Propagation/TwoRayGround   ;# radio-propagation model
set val(netif)          Phy/WirelessPhy            ;# network interface type
set val(mac)            Mac/802_11                 ;# MAC type
set val(ifq)            Queue/DropTail		   ;# interface queue type
set val(ll)             LL                         ;# link layer type
set val(ant)            Antenna/OmniAntenna        ;# antenna model
set val(ifqlen)         50                        ;# max packet in ifq
set val(rp)             DumbAgent                  ;# routing protocol
set val(nn)             3                         ;# number of mobilenodes
set val(x)		600
set val(y)		600

Mac/802_11 set dataRate_ 11Mb

#Phy/WirelessPhy set CSThresh_ 10.00e-12
#Phy/WirelessPhy set RXThresh_ 10.00e-11
#Phy/WirelessPhy set Pt_ 0.1
#Phy/WirelessPhy set Pt_ 7.214e-3

# Initialize Global Variables
set ns_		[new Simulator]
set tracefd     [open wifip2p_1.tr w]
$ns_ trace-all $tracefd

set namtrace [open wifip2p_1.nam w]
$ns_ namtrace-all-wireless $namtrace 100.0 100.0
#$ns_ use-newtrace

$ns_ eventtrace-all

# set up topography object
set topo       [new Topography]

$topo load_flatgrid $val(x) $val(y)

# Create God
create-god $val(nn)

# Create channel
set chan_1_ [new $val(chan)]

# Node config
#   For power management simulation, set -txPower, -rxPower,
#   -idlePower, -sleepPower, and -initialEnergy.
$ns_ node-config -adhocRouting $val(rp) \
    -llType $val(ll) \
    -macType $val(mac) \
    -ifqType $val(ifq) \
    -ifqLen $val(ifqlen) \
    -antType $val(ant) \
    -propType $val(prop) \
    -phyType $val(netif) \
    -topoInstance $topo \
    -agentTrace ON \
    -routerTrace ON \
    -macTrace ON \
    -movementTrace ON \
    -channel $chan_1_ \
    -energyModel EnergyModel \
    -txPower 0.660 \
    -rxPower 0.395 \
    -idlePower 0.035 \
    -sleepPower 0.001 \
    -initialEnergy 1000

#txPower 995.49
#rxPower 667.9
#idel   364.13
#idel   37

for {set i 0} {$i < [expr $val(nn)]} {incr i} {

    set node_($i) [$ns_ node]

    $node_($i) random-motion 0              ;# disable random motion

    set mac_($i) [$node_($i) getMac 0]

    $mac_($i) set RTSThreshold_ 3000
    
    $node_($i) set X_ [expr 50 + 10 * (cos($i * 2.0 * 3.1415 / $val(nn)))]
    $node_($i) set Y_ [expr 50 + 10 * (sin($i * 2.0 * 3.1415 / $val(nn)))]
    $node_($i) set Z_ 0.0

    $ns_ initial_node_pos $node_($i) 2
}


#Set Node 0 and Node $val(nn) as the APs. Thus the APs are the ends of the horizontal line. Each STA receives different power levels.

set AP_ADDR [$mac_(0) id]
$mac_(0) set AP_NoA_count 3
$mac_(0) set AP_NoA_Duration 0.01
$mac_(0) set AP_NoA_Interval 0.1
$mac_(0) set AP_NoA_Start 1.25
$mac_(0) ap $AP_ADDR
$mac_(0) set DTIMPeriod_ 1         ;# Set DTIM period
$mac_(0) set BeaconInterval_ 0.1


$mac_(1) ScanType PASSIVE
$mac_(2) ScanType PASSIVE

# Set STA's PS-mode
#  true: power save mode, false: no power save
$mac_(1) set isPowerSave_ false #true
$mac_(1) set isWFDPowerSave_ true

$mac_(2) set isPowerSave_ false #true
$mac_(2) set isWFDPowerSave_ true

Application/Traffic/CBR set packetSize_ 512
Application/Traffic/CBR set rate_ 256Kb

set udp [new Agent/UDP]
set cbr [new Application/Traffic/CBR]
$ns_ attach-agent $node_(1) $udp
$cbr attach-agent $udp

set null [new Agent/Null]
$ns_ attach-agent $node_(2) $null

$ns_ connect $udp $null

$ns_ at 1.0 "$cbr start"


$ns_ at 20.0 "puts \"NS EXITING...\""
$ns_ at 20.0 "stop"

$ns_ at 21.0 "$ns_ halt"

proc stop {} {
    global ns_ tracefd
    $ns_ flush-trace
    close $tracefd

    showBattery

    exit 0
}

proc showBattery {} {
    global ns_ node_ val
    for {set i 0} {$i < [expr $val(nn)]} {incr i} {
        # Print remaining energy of node
        puts [$node_($i) energy]
    }
}

showBattery

puts "Starting Simulation...*"
$ns_ run
